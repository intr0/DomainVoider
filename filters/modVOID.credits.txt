! modVOID combines the following lists with redundant rules removed.
! modVOID will be updated less frequently than certain contained lists, more often than others.
! @DomainVoider extends sincere appreciation to the original creators of all individual lists we include in modVOID.
! modVOID is an abbreviation of modernVOID - it is a subscription for those who enjoy the modern online experience without the inconveniences of ads or the worry of multiple threat vectors caused by that experience.
! modVOID does NOT interfere with social networking.

=========

! Filters included in modVOID v04.06.18 begin NOW!

! Title: AdGuard Simplified Domain Names filter
! Source: https://filters.adtidy.org/ios/filters/15.txt
! Homepage: https://github.com/AdguardTeam/AdguardDNS
! License: https://github.com/AdguardTeam/AdguardDNS/blob/master/LICENSE

=========

! Title: AdGuard Mobile Ads filter
! Source: https://filters.adtidy.org/ios/filters/11.txt
! Homepage: http://adguard.com/filters.html#mobile
! License: http://creativecommons.org/licenses/by-sa/3.0/

=========

! Title: AdGuard Spyware filter
! Source: https://filters.adtidy.org/ios/filters/3.txt
! Homepage: http://adguard.com/filters.html#privacy
! License: http://creativecommons.org/licenses/by-sa/3.0/

=========

! Title: AdGuard Annoyances filter
! Source: https://filters.adtidy.org/ios/filters/14.txt
! Homepage: http://adguard.com/filters.html#annoyances
! License: http://creativecommons.org/licenses/by-sa/3.0/

=========

! Title: Better.fyi | Trackers (List)
! Source: https://raw.githubusercontent.com/anarki999/Adblock-List-Archive/master/Better.fyiTrackersBlocklist.txt
! List from https://better.fyi/trackers/

=========

! Title: Fanboy’s Enhanced Tracking List
! Source: https://fanboy.co.nz/enhancedstats.txt
! Homepage: http://www.fanboy.co.nz/

=========

! Title: Blockzilla
! Source: https://raw.githubusercontent.com/zpacman/Blockzilla/master/Blockzilla.txt
! Homepage: https://blockzilla.jimdo.com/

=========

! Title: Minimal Hosts Blocker
! Source: https://raw.githubusercontent.com/arcetera/Minimal-Hosts-Blocker/master/etc/MinimalHostsBlocker/minimalhosts
! Homepage: https://github.com/arcetera/Minimal-Hosts-Blocker

=========

! Title: Hexxium Creations Threat List
! Source: https://raw.githubusercontent.com/HexxiumCreations/threat-list/gh-pages/hexxiumthreatlist.txt
! Homepage: https://hexxiumcreations.github.io/threat-list/
! Website: https://www.hexxiumcreations.com/projects/malicious-domain-blocking/

=========

! Title: CB Malicious Domains
! Source: https://raw.githubusercontent.com/cb-software/CB-Malicious-Domains/master/block_lists/adblock_plus.txt
! Homepage: https://github.com/cb-software/CB-Malicious-Domains

=========

! Title: CoinBlocker Domains List
! Source: https://raw.githubusercontent.com/ZeroDot1/CoinBlockerLists/master/list.txt
! Homepage: https://zerodot1.github.io/CoinBlockerListsWeb/

=========

! Title: Spam404
! Source: https://raw.githubusercontent.com/Dawsey21/Lists/master/adblock-list.txt
! Homepage: http://www.spam404.com/

=========

! Title: Anti-PopAds
! Source: https://raw.githubusercontent.com/Yhonay/antipopads/master/popads.txt
! Homepage: https://github.com/Yhonay/antipopads

=========

! Title: Adware filters
! Source: https://easylist-downloads.adblockplus.org/adwarefilters.txt
! Homepage: https://easylist.to/

=========

! Title: Adblock YouTube Ads
! Source: https://raw.githubusercontent.com/kbinani/adblock-youtube-ads/master/signed.txt
! Homepage: https://github.com/kbinani/adblock-youtube-ads

=========

! Title: Anti-Facebook List
! Source: https://fanboy.co.nz/fanboy-antifacebook.txt
! Homepage: http://www.fanboy.co.nz/

=========