## https://ko-fi.com/intr0

[![Latest Release](https://gitlab.com/intr0/DomainVoider/-/badges/release.svg)](https://gitlab.com/intr0/DomainVoider/-/releases)

## https://www.buymeacoffee.com/antinazi

[![pipeline status](https://gitlab.com/intr0/DomainVoider/badges/master/pipeline.svg)](https://gitlab.com/intr0/DomainVoider/-/commits/master)
***
#### Now proudly included on: [FilterLists.com](https://filterlists.com/).
**`https://filterlists.com/`**
***
## [DomainVoider](https://iosprivacy.com/domainvoider) now releases a single, simplified filter list. The effects of this change are far better loading times on all iDevices compatible with AdGuard Pro. It also reduces memory and CPU usage for those using it as a filterlist for the AdGuard or uBlock Orgin extensions.
### [AdGuard for iOS Pro](https://adguard.com/en/adguard-ios-pro/overview.html) is Free and Open-Source Software.
**`https://adguard.com/en/adguard-ios-pro/overview.html`**
***
### See the [Free Software Foundation](https://FSF.org/) to learn more.
**`https://FSF.org`**
***
## DomainVoider on GitLab
### [DomainVoider](https://gitlab.com/intr0/DomainVoider/raw/master/DomainVoider.txt)
**`https://gitlab.com/intr0/DomainVoider/raw/master/DomainVoider.txt`**
***
## DomainVoider on NotABug.org
### [DomainVoider](https://notabug.org/-intr0/DomainVoider/raw/master/DomainVoider.txt)
**`https://notabug.org/-intr0/DomainVoider/raw/master/DomainVoider.txt`**
***
